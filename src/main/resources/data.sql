INSERT INTO parameter (id, code, name, description, flag_active, create_by, create_date,update_by, update_date, version) 
       VALUES (14, 'REP001','Job Configure', 'Job Configure', '1', 'admin', NULL, 'admin', NULL, 0);

INSERT INTO parameter_detail (id, code, name, description, flag_active, variable1, variable2, variable3, variable4, variable5, variable6, variable7, variable8, variable9, variable10, parameter, create_by, create_date,update_by, update_date, version) 
	   VALUES (1401, 'TEMPLATE1', 'TEMPLATE1', 'TEMPLATE1', '1', '/usr/local/template/template1.xlsx', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14, 'admin', NULL, 'admin', NULL, 0);

INSERT INTO report_configuration(id,version,create_by,create_date,update_by,update_date,flag_active,code,name,file_name,sql_statement ,report_type , template_file_name)
       VALUES (1,0,'admin',NULL,NULL,NULL,'1','REPORT01','Report 01','REPORT01.xlsx','select * from parameter ' ,'xlsx' , NULL);

INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 1, 0 , 'admin', NULL , NULL , NULL , 1, '1', 'Code', 'CODE', 'CENTER' );
INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 2, 0 , 'admin', NULL , NULL , NULL , 1, '2', 'Name', 'NAME', 'LEFT' );
INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 3, 0 , 'admin', NULL , NULL , NULL , 1, '3', 'Description', 'DESCRIPTION', 'RIGHT' );

INSERT INTO report_configuration(id,version,create_by,create_date,update_by,update_date,flag_active,code,name,file_name,sql_statement ,report_type , template_file_name)
       VALUES (2,0,'admin',NULL,NULL,NULL,'1','REPORT02','Report 02','REPORT02.csv','select * from parameter ' ,'csv' , NULL);
       
INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 4, 0 , 'admin', NULL , NULL , NULL , 2, '1', 'Code', 'CODE', NULL );
INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 5, 0 , 'admin', NULL , NULL , NULL , 2, '2', 'Name', 'NAME', NULL );
INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 6, 0 , 'admin', NULL , NULL , NULL , 2, '3', 'Description', 'DESCRIPTION', NULL );


INSERT INTO report_configuration(id,version,create_by,create_date,update_by,update_date,flag_active,code,name,file_name,sql_statement ,report_type , template_file_name)
       VALUES (3,0,'admin',NULL,NULL,NULL,'1','REPORT03','Report 03','REPORT03.pdf','select * from parameter ' ,'pdf' , NULL);
       
INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 7, 0 , 'admin', NULL , NULL , NULL , 3, '1', 'Code', 'CODE', 'CENTER' );
INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 8, 0 , 'admin', NULL , NULL , NULL , 3, '2', 'Name', 'NAME', 'LEFT' );
INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 9, 0 , 'admin', NULL , NULL , NULL , 3, '3', 'Description', 'DESCRIPTION', 'RIGHT' );
        
INSERT INTO report_configuration(id,version,create_by,create_date,update_by,update_date,flag_active,code,name,file_name,sql_statement ,report_type , template_file_name)
       VALUES (4,0,'admin',NULL,NULL,NULL,'1','REPORT04','Report 04','REPORT04.pdf','select * from parameter ' ,'pdf' , NULL);
       
INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 10, 0 , 'admin', NULL , NULL , NULL , 4, '1', 'Code', 'CODE', 'CENTER' );
INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 11, 0 , 'admin', NULL , NULL , NULL , 4, '2', 'Name', 'NAME', 'LEFT' );
INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 12, 0 , 'admin', NULL , NULL , NULL , 4, '3', 'Description', 'DESCRIPTION', 'RIGHT' );
INSERT INTO  report_projection (id,version,create_by,create_date,update_by,update_date,report_configuration ,column_index,column_header_name,key_data,cell_align)
        VALUES( 13, 0 , 'admin', NULL , NULL , NULL , 4, '4', 'Version', 'Version', 'RIGHT' );

INSERT INTO report_configuration(id,version,create_by,create_date,update_by,update_date,flag_active,code,name,file_name,sql_statement ,report_type , template_file_name)
       VALUES (5,0,'admin',NULL,NULL,NULL,'1','REPORT05','Report 05','REPORT05.pdf','select * from parameter where code = :code' ,'pdf' , NULL);
              
