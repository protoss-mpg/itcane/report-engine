CREATE TABLE IF NOT EXISTS parameter (
	id bigint NOT NULL ,
	code varchar(255) NULL ,
	name varchar(255) NULL ,
	description varchar(255) NULL ,
	flag_active bit NULL ,
	create_by varchar(255) NULL ,
	create_date datetime2(7) NULL ,
	update_by varchar(255) NULL ,
	update_date datetime2(7) NULL ,
	version bigint NULL 
);

CREATE TABLE IF NOT EXISTS  parameter_detail (
	id bigint NOT NULL ,
	code varchar(255) NULL ,
	name varchar(255) NULL ,
	description varchar(255) NULL ,
	flag_active bit NULL ,
	variable1 varchar(255) NULL ,
	variable2 varchar(4000) NULL ,
	variable3 varchar(255) NULL ,
	variable4 varchar(255) NULL ,
	variable5 varchar(255) NULL ,
	variable6 varchar(255) NULL ,
	variable7 varchar(255) NULL ,
	variable8 varchar(255) NULL ,
	variable9 varchar(255) NULL ,
	variable10 varchar(255) NULL  ,
	parameter bigint NULL ,
	create_by varchar(255) NULL ,
	create_date datetime2(7) NULL ,
	update_by varchar(255) NULL ,
	update_date datetime2(7) NULL ,
	version bigint NULL
);

CREATE TABLE IF NOT EXISTS report_configuration (
	id bigint NOT NULL ,
	version bigint NULL ,
	create_by varchar(255) NULL ,
	create_date datetime2(7) NULL ,
	update_by varchar(255) NULL ,
	update_date datetime2(7) NULL ,
	flag_active bit NULL ,
	code varchar(255) NULL ,
	name varchar(255) NULL ,
	file_name varchar(255) NULL ,
	sql_statement varchar(255) NULL ,
	report_type varchar(255) NULL ,
	template_file_name varchar(255) NULL 
);

CREATE TABLE IF NOT EXISTS report_projection (
	id bigint NOT NULL ,
	version bigint NULL ,
	create_by varchar(255) NULL ,
	create_date datetime2(7) NULL ,
	update_by varchar(255) NULL ,
	update_date datetime2(7) NULL ,
	report_configuration bigint NULL ,
	column_index varchar(255) NULL ,
	column_header_name varchar(255) NULL ,
	key_data varchar(255) NULL ,
	cell_align varchar(255) NULL
);


