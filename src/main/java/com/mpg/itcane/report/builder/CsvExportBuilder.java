package com.mpg.itcane.report.builder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

import com.mpg.itcane.report.entity.ReportProjection;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CsvExportBuilder extends ExportBuilder{


	public CsvExportBuilder(String reportCode, String reportType) {
		super(reportCode, reportType);
	}
	
	private  CellProcessor[] getProcessors(List<String> keyColumnLs) {
		     CellProcessor[] processors = new CellProcessor[] {};
		     List<CellProcessor> processorLs = new ArrayList();
		     for(String keyColumn:keyColumnLs) {
		    	 processorLs.add(new NotNull());
		    	 log.debug(keyColumn);
		     }
	         return processorLs.toArray(processors);
	}

	@Override
	public ByteArrayOutputStream writeData(List<Map<String, Object>> dataLs)   throws IOException{
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		
		ICsvMapWriter mapWriter = new CsvMapWriter(new OutputStreamWriter(outputStream), CsvPreference.STANDARD_PREFERENCE);
                
		List<ReportProjection> projectionLs = new ArrayList(this.getReportProjectionSet());
		projectionLs.sort((ReportProjection m1, ReportProjection m2)->Integer.compare(Integer.parseInt(m1.getColumnIndex()), Integer.parseInt(m2.getColumnIndex())));
	   
        List<String> headerColumnLs = new ArrayList();
        List<String> keyColumnLs    = new ArrayList();
        for(ReportProjection projection:projectionLs) {
        	keyColumnLs.add(projection.getKeyData()); 		headerColumnLs.add(projection.getColumnHeaderName());
        }
        
        CellProcessor[] processors = getProcessors(keyColumnLs);
        
        try {
                // write the header
                mapWriter.writeHeader(headerColumnLs.toArray(new String[] {}));
                
                // write the customer lists
                for(Map<String, Object> record :dataLs) {
                	mapWriter.write(record,keyColumnLs.toArray(new String[] {}), processors);
                }
                
        }finally {
        	mapWriter.close();
        }
        
		return outputStream;
	}



}
