package com.mpg.itcane.report.builder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mpg.itcane.report.ApplicationConstant;
import com.mpg.itcane.report.entity.ReportProjection;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PdfExportBuilder extends ExportBuilder {

	public PdfExportBuilder(String reportCode, String reportType) {
		super(reportCode, reportType);
	}

	@Override
	public ByteArrayOutputStream writeData(List<Map<String, Object>> dataLs)  throws IOException, DocumentException{
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					// step 1 : Define Document
					Document document = new Document(PageSize.A4.rotate());
					// step 2 : Initial Writer
					PdfWriter.getInstance(document, outputStream);
					
					// step 3 : Open Ducument
					document.open();
					// step 4 : Define font
					BaseFont bf = BaseFont.createFont(ApplicationConstant.FONT_TH_NORMAL, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
					com.itextpdf.text.Font headFont = new com.itextpdf.text.Font(bf, 26);
					
					// step 5 : Set Report Name
					Paragraph reportName = new Paragraph(this.getReportName(),headFont); 
					reportName.setAlignment(Element.ALIGN_CENTER);
					document.add(reportName);
					document.add(new Paragraph(" "));

					// step 6 : Create table
					Integer numOfProjection = this.getReportProjectionSet().size();
					PdfPTable table = new PdfPTable(numOfProjection);
					if(numOfProjection > 3){
						table.setWidthPercentage(100f);
					}else{
						table.setWidthPercentage(20f);
					}
					
					// step 7 : Set Header
					PdfPCell cell = null;
		            Map<String,Integer> mapKeyIndex = new HashMap(); 
		            Map<String,String> mapKeyType = new HashMap();
					for(ReportProjection reportProjection:this.getReportProjectionSet()){
					  	cell = new PdfPCell();
						cell.setPhrase(new Paragraph(reportProjection.getColumnHeaderName(),new com.itextpdf.text.Font(bf, 18)));
					    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					    cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
					    table.addCell(cell);
					    
					    mapKeyIndex.put(reportProjection.getKeyData(), Integer.parseInt(reportProjection.getColumnIndex()));
					    mapKeyType.put(reportProjection.getKeyData(), reportProjection.getCellAlign());
					  }
					
					// step 8 : Set Data
					table.getDefaultCell();
					for(Map<String,Object> model:dataLs){
						for(String key:mapKeyIndex.keySet()){
							cell = new PdfPCell();
							cell.setPhrase(new Paragraph(String.valueOf(model.get(key)),new com.itextpdf.text.Font(bf, 16)));
				  	      	
			  	              if(ApplicationConstant.REPORT_COLUMN_ALIGN_LEFT.equals(mapKeyType.get(key))){
			  	            	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			  	              }else if(ApplicationConstant.REPORT_COLUMN_ALIGN_RIGHT.equals(mapKeyType.get(key))){
			  	            	cell.setHorizontalAlignment(Element.ALIGN_RIGHT );
			  	              }else {
			  	            	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			  	              }
			  	            table.addCell(cell);
			  	      	}
			  	      
					}
					document.add(table);
					
					// step 9: Close Document
					document.close();

    	
		return outputStream;
	}


}
