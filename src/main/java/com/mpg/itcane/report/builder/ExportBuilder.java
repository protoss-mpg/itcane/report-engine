package com.mpg.itcane.report.builder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.itextpdf.text.DocumentException;
import com.mpg.itcane.report.ApplicationConstant;
import com.mpg.itcane.report.entity.ReportProjection;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public abstract class ExportBuilder {
	
	
	private ByteArrayOutputStream outputStream;
	private String fileName;
	private String contentType;
	private String reportCode;
	private List<Map<String,Object>> dataList;
	private Set<ReportProjection> reportProjectionSet ;
	private String reportName ;
	
	
	public ExportBuilder(String reportCode,String reportType) {
		 switch (reportType){
	         case ApplicationConstant.EXPORT_TYPE_EXCEL :
	        	 this.contentType = ApplicationConstant.CONTENT_TYPE_EXCEL;
	        	 this.reportCode = reportCode;
	             break;
	         case ApplicationConstant.EXPORT_TYPE_PDF :
	        	 this.contentType = ApplicationConstant.CONTENT_TYPE_PDF;
	        	 this.reportCode = reportCode;
	             break;
	         default:
	        	 this.contentType = ApplicationConstant.CONTENT_TYPE_CSV;
	        	 this.reportCode = reportCode;
	             break;
	     }
	}
	

	public ExportBuilder setDataListBuilder(List<Map<String, Object>> dataList) {
		this.dataList = dataList;
		return this;
	}
	
	

	public ExportBuilder setReportProjectionSet(Set<ReportProjection> reportProjectionSet) {
		this.reportProjectionSet = reportProjectionSet;
		return this;
	}


	public ExportBuilder setReportName(String reportName) {
		this.reportName = reportName;
		return this;
	}
	

	public ExportBuilder setFileName(String fileName) {
		this.fileName = fileName;
		return this;
	}


	public boolean isGenerateSuccess() throws IOException, DocumentException{
		boolean hasNotError = true;
		outputStream = writeData(dataList); 
		
		return hasNotError;
		
	}
	
	public abstract ByteArrayOutputStream writeData(List<Map<String,Object>> dataLs) throws IOException, DocumentException ;
	
	

}
