package com.mpg.itcane.report.builder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.itextpdf.text.DocumentException;
import com.mpg.itcane.report.ApplicationConstant;
import com.mpg.itcane.report.entity.ReportProjection;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExcelExportBuilder extends ExportBuilder{

	public ExcelExportBuilder(String reportCode, String reportType) {
		super(reportCode, reportType);
	}

	@Override
	public ByteArrayOutputStream writeData(List<Map<String, Object>> dataLs) throws IOException, DocumentException {
		      /* Write to outPutStream */
		  ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		  Workbook wb = new XSSFWorkbook();
	  	  Sheet sheet = wb.createSheet("REPORT");
	  	  
	  	  /* Set Header */
		      Font fontHeader = wb.createFont();
		      fontHeader.setFontName("Arial");
		      fontHeader.setBold(true);
		      
		      
	  	  CellStyle styleReportName = wb.createCellStyle();
	  	  styleReportName.setFillForegroundColor(IndexedColors.WHITE.getIndex());
	  	  styleReportName.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	  	  styleReportName.setAlignment(HorizontalAlignment.CENTER);
	  	  styleReportName.setFont(fontHeader);
	  	  
	  	  Row rowReportName = sheet.createRow(1);
	  	  Cell cellReportName = rowReportName.createCell(4);
	          cellReportName.setCellValue(new XSSFRichTextString(this.getReportName()));
	          cellReportName.setCellStyle(styleReportName);
	  	  
	  	  
		      // Create a row and put some cells in it. Rows are 0 based.
		      Row row = sheet.createRow(4);
		      
		      CellStyle styleHeader = wb.createCellStyle();
		      styleHeader.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
		      styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		      styleHeader.setAlignment(HorizontalAlignment.CENTER);
		      styleHeader.setFont(fontHeader);
		    
		      
		      Cell cell = null;
		      Map<String,Integer> mapKeyIndex = new HashMap(); 
		      Map<String,String>  mapKeyType  = new HashMap(); 
		      int countColumn = 0;
		      
		      for(ReportProjection reportProjection:this.getReportProjectionSet()){
		      	cell = row.createCell(Integer.parseInt(reportProjection.getColumnIndex()));
		          cell.setCellValue(new XSSFRichTextString(reportProjection.getColumnHeaderName()));
		          cell.setCellStyle(styleHeader);
		          
		          mapKeyIndex.put(reportProjection.getKeyData(), Integer.parseInt(reportProjection.getColumnIndex()));
		          mapKeyType.put(reportProjection.getKeyData(), reportProjection.getCellAlign());
		          
		          countColumn++;
		      }
		      
		      /*Set Data*/
		      int rowIndex = 5;
		      for(Map<String,Object> model:dataLs){
		      	Row rowData = sheet.createRow(rowIndex);
		      	
		      	for(Entry<String, Integer> entry:mapKeyIndex.entrySet()){
		      		String key = entry.getKey();
		      		cell = rowData.createCell(mapKeyIndex.get(key));
		      		  String content = null;
		      		  if(model.get(key) !=null) {
		      			content = String.valueOf(model.get(key));
		      		  }
		      		  cell.setCellValue(new XSSFRichTextString( content));
		              
		              CellStyle styleCell = wb.createCellStyle();
		              if(ApplicationConstant.REPORT_COLUMN_ALIGN_LEFT.equals(mapKeyType.get(key))){
		              	styleCell.setAlignment(HorizontalAlignment.LEFT);
		              	cell.setCellStyle(styleCell);
		              }else if(ApplicationConstant.REPORT_COLUMN_ALIGN_RIGHT.equals(mapKeyType.get(key))){
		              	styleCell.setAlignment(HorizontalAlignment.RIGHT);
		              	cell.setCellStyle(styleCell);
		              }else {
		              	styleCell.setAlignment(HorizontalAlignment.CENTER);
		              	cell.setCellStyle(styleCell);
		              }
		      	}
		      	rowIndex++;
		      }
		      
		      //autoSizeColumn
		      for(int colNum = 0; colNum<countColumn;colNum++)   
		    	  wb.getSheetAt(0).autoSizeColumn(colNum);
		      
		    
			try {
				wb.write(outputStream);
			}finally {
				wb.close();
			}
			
		return outputStream;
	}


}
