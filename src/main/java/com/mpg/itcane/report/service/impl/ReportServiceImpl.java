package com.mpg.itcane.report.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.mpg.itcane.report.ExportFactory;
import com.mpg.itcane.report.builder.ExportBuilder;
import com.mpg.itcane.report.entity.ReportConfiguration;
import com.mpg.itcane.report.entity.ReportProjection;
import com.mpg.itcane.report.model.ResponseModel;
import com.mpg.itcane.report.repository.ParameterDetailRepository;
import com.mpg.itcane.report.repository.ParameterRepository;
import com.mpg.itcane.report.repository.ReportConfigurationRepository;
import com.mpg.itcane.report.service.ReportService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ReportServiceImpl implements ReportService {

	 @Autowired
	 ParameterRepository parameterRepository;

	 @Autowired
	 ParameterDetailRepository parameterDetailRepository;
	 
	 @Autowired
	 ReportConfigurationRepository reportConfigurationRepository;
	 
	 @Autowired
	 NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Override
	public ResponseEntity genReportByReportCode(String reportCode,Map<String,String> mapParam) {
		ResponseModel response = new ResponseModel();
		try {
			ReportConfiguration reportConfiguration = reportConfigurationRepository.findFirstByCode(reportCode);
			List<Map<String,Object>> dataList = getDataList(mapParam,reportConfiguration);
			String fileName = reportConfiguration.getFileName();
			String reportName = reportConfiguration.getName();
			String reportType = reportConfiguration.getReportType();
			Set<ReportProjection> reportProjectionSet = reportConfiguration.getProjections();
			 
			  
			ExportBuilder builder = ExportFactory.builder(reportCode,reportType)
					.setDataListBuilder(dataList)
					.setReportProjectionSet(reportProjectionSet)
					.setReportName(reportName)
					.setFileName(fileName);
			
			
			if(dataList.isEmpty()) {
				return ResponseEntity.noContent().build();
			}
			
			if(builder.isGenerateSuccess()) {
				return getResponseEntity(builder.getOutputStream(),builder.getFileName(),builder.getContentType());
			}
		} catch (Exception e) {
			log.error("{}", e);
			response.setSuccess(false);
			response.setMessage(e.getMessage());
		}

		return ResponseEntity.ok(response);
	}
	
	private Map<String,Boolean> findSqlParam(String sql){
		Map<String,Boolean> sqlParam = new HashMap();
    	Pattern findParametersPattern = Pattern.compile("(?<!')(:[\\w]*)(?!')");
    	Matcher matcher = findParametersPattern.matcher(sql);
    	while (matcher.find()) { 
    		sqlParam.put(String.valueOf((matcher.group().substring(1))), true);
    	}
    	
    	return sqlParam;
	}
	
	private MapSqlParameterSource getMapSqlParameterSource(Map<String,String> mapParam,Map<String,Boolean> sqlParam) {
		MapSqlParameterSource msps = new MapSqlParameterSource();
		for(Entry<String, String> entry:mapParam.entrySet()){
			String key   = entry.getKey();
			String value = mapParam.get(key);
			
			Boolean isMatchParam = sqlParam.get(key);
			if(isMatchParam.equals(true)) {
				msps.addValue(key,value);
			}
			
		}
		
		return msps;
	}
	
	private List<Map<String,Object>> getDataList(Map<String,String> mapParam,ReportConfiguration reportConfiguration){
		
		String sql = reportConfiguration.getSqlStatement();
		
		Map<String,Boolean> sqlParam = findSqlParam(sql);
		
		MapSqlParameterSource msps = this.getMapSqlParameterSource(mapParam, sqlParam);
		
		
		return namedParameterJdbcTemplate.queryForList(sql, msps);
	}


	private ResponseEntity<Resource> getResponseEntity(ByteArrayOutputStream outputStream,String fileName,String contentType) {
		
		/* Write to outPutStream */
    	ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        HttpHeaders headers = new HttpHeaders();
			headers.setContentType(new MediaType("application", contentType));
	        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+fileName+"\"");
	        headers.setContentLength(outputStream.size());
        return ResponseEntity
	              .ok()
	              .headers(headers)
	              .body(new InputStreamResource(inputStream));
	}
	 
	 
	 
	 

}
