package com.mpg.itcane.report.service;

import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface ReportService {

	public ResponseEntity genReportByReportCode(String reportCode,Map<String,String> mapParam);
	
	
}
