package com.mpg.itcane.report.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.mpg.itcane.report.service.ReportService;
import com.protosstechnology.commons.util.RequestUtil;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api("Service Of Generate Report")
@Controller
public class ExportReportController {
	
	@Autowired
	ReportService reportService;
	
	@GetMapping("/export/{reportCode}")
    public ResponseEntity exportReport(HttpServletRequest request,@PathVariable String reportCode) {
		Map<String,String> mapParam = RequestUtil.getMapParam(request.getParameterMap());
		return reportService.genReportByReportCode(reportCode,mapParam);
		
	}
	
	

}
