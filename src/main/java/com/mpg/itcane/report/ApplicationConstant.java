package com.mpg.itcane.report;

public class ApplicationConstant {
	
	private ApplicationConstant() {
		super();
	}
	
	
	/* ERROR CODE */
	public static final String ERROR_CODE_PROCESS_FAIL        = "REPERR001";
	public static final String ERROR_CODE_THROW_EXCEPTION     = "REPERR002";
	public static final String ERROR_CODE_NO_DATA_FOUND       = "REPERR003";
	public static final String ERROR_CODE_REQUIRE_DATA        = "REPERR004";
	
	public static final String EXPORT_TYPE_EXCEL              = "xlsx";
	public static final String EXPORT_TYPE_PDF                = "pdf";
	public static final String EXPORT_TYPE_CSV                = "csv";
	

	public static final String CONTENT_TYPE_EXCEL             = "vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	public static final String CONTENT_TYPE_PDF               = "pdf";
	public static final String CONTENT_TYPE_CSV               = "csv";
	
	public static final String REPORT_COLUMN_ALIGN_LEFT       = "LEFT";
	public static final String REPORT_COLUMN_ALIGN_CENTER     = "CENTER";
	public static final String REPORT_COLUMN_ALIGN_RIGHT      = "RIGHT";
	
	public static final String FONT_TH_NORMAL      			  = "font/THSarabunNew.ttf";
	public static final String FONT_TH_BOLD        			  = "font/THSarabunNew Bold.ttf";
	public static final String FONT_TH_BOLD_ITALIC 			  = "font/THSarabunNew BoldItalic.ttf";
	public static final String FONT_TH_ITALIC      			  = "font/THSarabunNew Italic.ttf";
}
