package com.mpg.itcane.report;

import com.mpg.itcane.report.builder.CsvExportBuilder;
import com.mpg.itcane.report.builder.ExcelExportBuilder;
import com.mpg.itcane.report.builder.ExportBuilder;
import com.mpg.itcane.report.builder.PdfExportBuilder;

public class ExportFactory {
	
	private ExportFactory(){
		super();
    }

    public static ExportBuilder builder(String reportCode,String reportType){
    	ExportBuilder exportBuilder = null;
        switch (reportType){
            case ApplicationConstant.EXPORT_TYPE_EXCEL :
            	exportBuilder = new ExcelExportBuilder(reportCode,reportType);
                break;
            case ApplicationConstant.EXPORT_TYPE_PDF :
            	exportBuilder = new PdfExportBuilder(reportCode,reportType);
                break;
            default:
            	exportBuilder = new CsvExportBuilder(reportCode,reportType);
                break;
        }
        return  exportBuilder;
    }

}
