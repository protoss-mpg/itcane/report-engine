package com.mpg.itcane.report.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;

import com.mpg.itcane.report.entity.ReportConfiguration;

public interface ReportConfigurationRepository extends JpaSpecificationExecutor<ReportConfiguration>, JpaRepository<ReportConfiguration, Long> {
	
	ReportConfiguration findFirstByCode(@Param("code") String code);
}
