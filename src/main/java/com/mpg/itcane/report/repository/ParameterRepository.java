package com.mpg.itcane.report.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mpg.itcane.report.entity.Parameter;

public interface ParameterRepository extends JpaSpecificationExecutor<Parameter>, JpaRepository<Parameter, Long> {
}
