package com.mpg.itcane.report;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class EngineApplicationTests {
	
	@Autowired
	 private WebApplicationContext wac;
	 private MockMvc mockMvc;
	 
	 @Before
	 public void setUp(){
	        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	 }

	 @After
	 public void tearDown() {
	 }

	@Test
	public void exportExcelSimpleBasic() throws Exception {
		String reportCode = "REPORT01";
		MockHttpServletResponse response = mockMvc.perform(get("/export/"+reportCode) )
                .andExpect(status().isOk())
                .andReturn().getResponse();
		
        Assert.assertNotEquals(0, response.getContentAsByteArray().length);
        Assert.assertEquals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", response.getContentType());
        Assert.assertEquals("attachment; filename=\"REPORT01.xlsx\"", response.getHeader("content-disposition"));
	}
	
	@Test
	public void exportCsvSimpleBasic() throws Exception {
		String reportCode = "REPORT02";
		MockHttpServletResponse response = mockMvc.perform(get("/export/"+reportCode) )
                .andExpect(status().isOk())
                .andReturn().getResponse();
		
        Assert.assertNotEquals(0, response.getContentAsByteArray().length);
        Assert.assertEquals("application/csv", response.getContentType());
        Assert.assertEquals("attachment; filename=\"REPORT02.csv\"", response.getHeader("content-disposition"));
	}
	
	@Test
	public void exportPdfSimpleBasic() throws Exception {
		String reportCode = "REPORT03";
		MockHttpServletResponse response = mockMvc.perform(get("/export/"+reportCode) )
                .andExpect(status().isOk())
                .andReturn().getResponse();
		
        Assert.assertNotEquals(0, response.getContentAsByteArray().length);
        Assert.assertEquals("application/pdf", response.getContentType());
        Assert.assertEquals("attachment; filename=\"REPORT03.pdf\"", response.getHeader("content-disposition"));
	}
	

	@Test
	public void exportPdfSimpleBasicMoreThanThreeColumn() throws Exception {
		String reportCode = "REPORT04";
		MockHttpServletResponse response = mockMvc.perform(get("/export/"+reportCode) )
                .andExpect(status().isOk())
                .andReturn().getResponse();
		
        Assert.assertNotEquals(0, response.getContentAsByteArray().length);
        Assert.assertEquals("application/pdf", response.getContentType());
        Assert.assertEquals("attachment; filename=\"REPORT04.pdf\"", response.getHeader("content-disposition"));
	}
	

	@Test
	public void exportPdfSimpleBasicNoContent() throws Exception  {
		String reportCode = "REPORT05";
		mockMvc.perform(get("/export/"+reportCode)
				.param("code", "fakeCode"))
                .andExpect(status().isNoContent());
		
	}
	
	@Test
	public void exportPdfSimpleBasicNoProjectionSetup() throws Exception {
		String reportCode = "REPORT05";
		MockHttpServletResponse response = mockMvc.perform(get("/export/"+reportCode)
				.param("code", "REP001"))
				.andExpect(status().isOk())
				.andExpect(content().json(
		        		"{"
		        		+ "\"success\":false,"
		        		+ "\"message\":\"The number of columns in PdfPTable constructor must be greater than zero.\""
		        		+ "}"
		        		))
                .andReturn().getResponse();
	}
	

}




